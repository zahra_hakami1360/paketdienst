package z.hakami.paketdienst;

/**
 * PaketActivity
 * @version 12.05.2021
 * @autor  Zahra Hakami
 */

import androidx.appcompat.app.AppCompatActivity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class PaketActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paket);

        //Dem Button einen Eventlistner mitgeben über die eigenen Klasse
        //1. Die Klasse muss das Interface View.OnClickListener implementieren
        // und darüber die Methode onClick(View view) --> implements View.OnClickListener
        //2. Das Viewelement holen
        Button button = findViewById(R.id.btnCalculate);
        //3. dem ViewElement den EventListenerzuweisen
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        view.setBackgroundColor(Color.RED);
        //Paket instanzieren getPrice und getVolum aufrufen
        //Eingegebenen Zahlen holen

        //TODO Validierung!
        EditText editText = findViewById(R.id.editTextWeight);
        //Holen den wert aus dem EditText(aus feld)((nur nummer als String))
        //ziffer ist hier unsere eingabe
        String ziffer = editText.getText().toString();
        //String(ziffer) in double Casten
        double weight = Double.parseDouble(ziffer);

        //Log.i("Gewicht",String.valueOf(weight));

        //leer varibel für ausgabe
        String message = "";

        TextView textView = findViewById(R.id.txtOutputPrice);
        //Paketobjekt erzeugen ist(Aber ist richtig, in der nächsten Zeile wird ein Objekt erzeugt)
        Parcel parcel = new Parcel(weight);
        //Wenn das gewicht vom paket valide...
        if (parcel.isValidWeight(weight)) {
            //... dann  berechne preic(von seinem gewicht)
            double price = parcel.calculatePrice(weight);
            Log.i("Price", String.valueOf(price));
            message = "Kosten: " + String.valueOf(price) + " €";
        } else {
            //...sonst gib Fehler meldungl
            message = "Ihr Paket ist zu schwer und kann nicht versendet werden";
            Log.i("ErrorMessage", message);
        }

        textView.setText(message);
    }
}