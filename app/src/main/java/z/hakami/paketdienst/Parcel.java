package z.hakami.paketdienst;

public class Parcel {
    private double weight;
    private double height;
    private double width;
    private double deepth;
    //Constructor
    public Parcel(double weight){
        this.weight = weight;
    }
    public Parcel(double weight, double height, double width, double deepth){
        this.weight = weight;
        this.height = height;
        this.width = width;
        this.deepth = deepth;
    }
    //Functions
    boolean isValidWeight(double weight) {
        return (weight > 0.00 && weight <= 20.00);
    }

    public double calculatePrice(double weight) {
        double price;
        if (weight <= 5) {
            price = 7.00;
        } else if (weight <= 10) {
            price = 10.50;
        } else {
            price = 14.00;
        }
        return price;
    }
    public double calculateVolume(){
        double volume;
        volume = weight * height * width;
        return volume;
    }

    //Getter und Setter
    public void setWeight(double weight) {
        this.weight = weight;
    }
    public double getWeight() {
        return this.weight;
    }
    public double getHeight() {
        return height;
    }
    public void setHeight(double height) {
        this.height = height;
    }
    public double getWidth() {
        return width;
    }
    public void setWidth(double width) {
        this.width = width;
    }
    public double getDeepth() {
        return deepth;
    }
    public void setDeepth(double deepth) {
        this.deepth = deepth;
    }
}